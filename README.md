# mos-management

Sample project that builds a macOS management stack based on Docker, munki, munkireport-php and plenty other open-source software.

## About the stack

This stack is based on customized Docker image (first pulls GNU/Linux Ubuntu 18.04 LTS and then applying some extras mods on top.
