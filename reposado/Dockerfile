#
# macOS management system
#

# Pull base image Ubuntu 18.04 LTS.
FROM ubuntu:18.04
LABEL maintainer "Mickael Masquelin <mickael.masquelin@univ-lille.fr>"

# Define the some ENV variables
ARG DEBIAN_FRONTEND=noninteractive

ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8

ENV TZ Europe/Paris

ENV NGINX_WORKER_PROCESSES 1
ENV NGINX_MAX_UPLOAD 0
ENV HTPASSWD YouNeedToChangeThisValue

ENV nginx_repo_vhost /etc/nginx/sites-available/sus

ENV nginx_conf /etc/nginx/nginx.conf
ENV supervisor_conf /etc/supervisor/conf.d/supervisord.conf

# Enable Ubuntu Universe, Multiverse, and deb-src for main.
RUN sed -i 's/^#\s*\(deb.*main restricted\)$/\1/g' /etc/apt/sources.list \
  && sed -i 's/^#\s*\(deb.*universe\)$/\1/g' /etc/apt/sources.list \
  && sed -i 's/^#\s*\(deb.*multiverse\)$/\1/g' /etc/apt/sources.list

# Fix some issues with APT packages.
# See https://github.com/dotcloud/docker/issues/1024
RUN apt-get update -q --fix-missing \
  && apt-get install -y --no-install-recommends apt-utils \
  && dpkg-divert --local --rename --add /sbin/initctl \
  && ln -sf /bin/true /sbin/initctl

# Upgrading dist image
RUN apt-get dist-upgrade -y --no-install-recommends -o Dpkg::Options::="--force-confold"

# Install HTTPS support for APT and somes fixes
RUN apt-get install -y apt-utils apt-transport-https ca-certificates

# Install nginx, php-fpm, some php modules and supervisord from Ubuntu repo
RUN apt-get install -y nginx supervisor gettext-base

# Install python tools
RUN apt-get install -y python-pip python-dev

# Add some required software
RUN apt-get install -y git curl build-essential apache2-utils unzip \
  && rm -fr /var/lib/apt/lists/*

# Copying some virtualhost and configuration files definition
COPY nginx/sus ${nginx_repo_vhost}
COPY nginx/upload.conf /etc/nginx/conf.d/upload.conf

# Enable php-fpm on nginx virtualhost configuration and set timezone
RUN ln -snf /usr/share/zoneinfo/${TZ} /etc/localtime && echo ${TZ} > /etc/timezone && \
  echo "\ndaemon off;" >> ${nginx_conf}

# Copy supervisor configuration
COPY supervisord.conf ${supervisor_conf}

# Modify the number of workers for Nginx (defaut is auto)
# See https://wardsparadox.github.io/2018/01/29/faster-munki-downloads-from-nginx/
RUN sed -i "/worker_processes\s/c\worker_processes ${NGINX_WORKER_PROCESSES};" /etc/nginx/nginx.conf
RUN sed -i "/client_max_body_size\s/c\client_max_body_size ${NGINX_MAX_UPLOAD};" /etc/nginx/conf.d/upload.conf

# Adding a munki user for macOS app management
RUN addgroup --system munki  --gid 5000 \
  && adduser --system munki --ingroup munki \
  && usermod -a -G munki www-data

# Generating .htpasswd file for protecting munki web server folders
RUN htpasswd -cb /etc/nginx/.htpasswd munki ${HTPASSWD}

# Making base folders for Munki and settings some perms
RUN mkdir -p /run/php && \
  chown -R www-data:www-data /run/php

# Cloning both reposado and margarita project, fixing some perms issues and install pip :)
RUN bash -c "mkdir -pv /usr/local/asus" && \
  git clone https://github.com/wdas/reposado.git /usr/local/asus/reposado && \
  bash -c "mkdir -pv /usr/local/asus/{datas,logs}"

COPY reposado/preferences.plist /usr/local/asus/reposado/code/preferences.plist

RUN  chown -R www-data:munki /usr/local/asus && \
  mkdir -p /usr/local/asus/datas/{html,metadata} && \
  chmod -R 2774 /usr/local/asus

# Installing flask, simplejson
RUN pip install flask && \
  pip install simplejson

# Forwarding requests and errors logs to docker log collector
RUN rm -f /var/log/nginx/access-sus.log /var/log/nginx/error-sus.log && \
  ln -sf /dev/stdout /var/log/nginx/access-sus.log && \
  ln -sf /dev/stderr /var/log/nginx/error-sus.log

# Some extra symlinks
RUN ln -s /etc/nginx/sites-available/sus /etc/nginx/sites-enabled/sus && \
  rm -f /etc/nginx/sites-enabled/default

COPY start.sh /start.sh
CMD [ "./start.sh" ]
