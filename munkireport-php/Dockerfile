#
# macOS management system
#

# Pull base image Ubuntu 18.04 LTS.
FROM ubuntu:18.04
LABEL maintainer "Mickael Masquelin <mickael.masquelin@univ-lille.fr>"

# Define the some ENV variables
ARG DEBIAN_FRONTEND=noninteractive
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8

ENV MR_SITENAME MunkiReport
ENV MR_MODULES ard, bluetooth, disk_report, munkireport, managedinstalls, munkiinfo, network, security, warranty
ENV TZ Europe/Paris

ENV NGINX_WORKER_PROCESSES 1
ENV NGINX_MAX_UPLOAD 0
ENV HTPASSWD YouNeedToChangeThisValue

ENV nginx_conf /etc/nginx/nginx.conf
ENV nginx_def_vhost /etc/nginx/sites-available/mr-php
ENV php_conf /etc/php/7.2/fpm/php.ini
ENV supervisor_conf /etc/supervisor/conf.d/supervisord.conf

# Enable Ubuntu Universe, Multiverse, and deb-src for main.
RUN sed -i 's/^#\s*\(deb.*main restricted\)$/\1/g' /etc/apt/sources.list \
  && sed -i 's/^#\s*\(deb.*universe\)$/\1/g' /etc/apt/sources.list \
  && sed -i 's/^#\s*\(deb.*multiverse\)$/\1/g' /etc/apt/sources.list

# Fix some issues with APT packages.
# See https://github.com/dotcloud/docker/issues/1024
RUN apt-get update -q --fix-missing \
  && apt-get install -y --no-install-recommends apt-utils \
  && dpkg-divert --local --rename --add /sbin/initctl \
  && ln -sf /bin/true /sbin/initctl

# Upgrading dist image
RUN apt-get dist-upgrade -y --no-install-recommends -o Dpkg::Options::="--force-confold"

# Install HTTPS support for APT and somes fixes
RUN apt-get install -y apt-utils apt-transport-https ca-certificates

# Install nginx, php-fpm, some php modules and supervisord from Ubuntu repo
RUN apt-get install -y nginx supervisor gettext-base php7.2-fpm php7.2-ldap php7.2-zip php7.2-xml php7.2-soap php7.2-mbstring php7.2-mysql php7.2-cgi

# Add some required software
RUN apt-get install -y git curl build-essential apache2-utils unzip \
  && rm -fr /var/lib/apt/lists/*

# Copying some virtualhost and configuration files definition
COPY nginx/mr-php ${nginx_def_vhost}
COPY nginx/upload.conf /etc/nginx/conf.d/upload.conf

# Enable php-fpm on nginx virtualhost configuration and set timezone
RUN sed -i -e 's/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/g' ${php_conf} && \
  ln -snf /usr/share/zoneinfo/${TZ} /etc/localtime && echo ${TZ} > /etc/timezone && \
  echo "\ndaemon off;" >> ${nginx_conf}

# Copy supervisor configuration
COPY supervisord.conf ${supervisor_conf}

# Modify the number of workers for Nginx (defaut is auto)
# See https://wardsparadox.github.io/2018/01/29/faster-munki-downloads-from-nginx/
RUN sed -i "/worker_processes\s/c\worker_processes ${NGINX_WORKER_PROCESSES};" /etc/nginx/nginx.conf
RUN sed -i "/client_max_body_size\s/c\client_max_body_size ${NGINX_MAX_UPLOAD};" /etc/nginx/conf.d/upload.conf

# Adding a munki user for macOS app management
RUN addgroup --system munki \
  && adduser --system munki --ingroup munki \
  && usermod -a -G munki www-data

# Generating .htpasswd file for protecting munki web server folders
RUN htpasswd -cb /etc/nginx/.htpasswd munki ${HTPASSWD}

# Cloning munkireport project, fixing some perms, install some composer tools and then fixing again :)
RUN git clone https://github.com/munkireport/munkireport-php /usr/local/munkireport && \
  chown -R www-data:munki /usr/local/munkireport && \
  chmod -R 2774 /usr/local/munkireport && \
  su -s /bin/bash -c "php -r \"copy('https://getcomposer.org/installer', '/usr/local/munkireport/composer-setup.php');\"" munki && \
  su -s /bin/bash -c "php /usr/local/munkireport/composer-setup.php --install-dir=/usr/local/munkireport" munki && \
  su -s /bin/bash -c "php -r \"unlink('/usr/local/munkireport/composer-setup.php');\"" munki && \
  su -s /bin/bash -c "php /usr/local/munkireport/composer.phar install --no-dev --no-suggest --optimize-autoloader --working-dir=/usr/local/munkireport" munki

COPY munkireport/phpdotenv /usr/local/munkireport/.env

RUN chown -R www-data:munki /usr/local/munkireport && \
  chmod -R 775 /usr/local/munkireport && \
  ln -s /usr/local/munkireport/public /usr/share/nginx/html/munkireport

# Making base folders for Munki and settings some perms
RUN mkdir -p /run/php && \
  chown -R www-data:www-data /run/php

# Forwarding requests and errors logs to docker log collector
RUN rm -f /var/log/nginx/access-mr4php.log /var/log/nginx/error-mr4php.log && \
  ln -sf /dev/stdout /var/log/nginx/access-mr4php.log && \
  ln -sf /dev/stderr /var/log/nginx/error-mr4php.log

# Some extra symlinking op
RUN ln -s /etc/nginx/sites-available/mr-php /etc/nginx/sites-enabled/mr-php && \
  rm -f /etc/nginx/sites-enabled/default

COPY start.sh /start.sh
CMD [ "./start.sh" ]
